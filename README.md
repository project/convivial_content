# Convivial Content

Ships default content for convivial profile.

## Table of contents

- Requirements
- Installation
- Configuration
- FAQ
- Maintainers


## Requirements

This module requires the following modules:
- [Convivial Core](https://www.drupal.org/project/convivial_core)

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend
2. Go to Administration > Configuration > Convivial CXP > Content Import > Convivial Content
3. Configure the **Source URL** to fetch the source content
4. Go to Administration > Configuration > Convivial CXP > Content Import
5. Select the dataset and import the content

## FAQ

- Q: Why importing is not working with Authenticated user?
- A: The import requires high-level permissions, so always perform imports with the site administrator role.

## Maintainers

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
