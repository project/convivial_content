<?php

namespace Drupal\convivial_content\Commands;

use Drupal\convivial_content\DataImporter;
use Drupal\convivial_content\DataSourceManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Convivial Content Drush command file.
 */
class ConvivialContentCommands extends DrushCommands {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Helper Importer Service.
   *
   * @var \Drupal\convivial_content\DataImporter
   */
  protected $dataImporter;

  /**
   * Helper for Sourcing the sites.
   *
   * @var \Drupal\convivial_content\DataSourceManager
   */
  protected $dataSourceManager;

  /**
   * Constructs a new ConvivialContentCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\convivial_content\DataImporter $dataImporter
   *   The Helper service for Importing contents.
   * @param \Drupal\convivial_content\DataSourceManager $dataSourceManager
   *   A service that retrieves YAML file content from a specified URL.
   */
  public function __construct(ConfigFactoryInterface $configFactory, DataImporter $dataImporter, DataSourceManager $dataSourceManager) {
    parent::__construct();
    $this->configFactory = $configFactory;
    $this->dataImporter = $dataImporter;
    $this->dataSourceManager = $dataSourceManager;
  }

  /**
   * Import content.
   *
   * @param string $dataset
   *   The Dataset (machine name) of the specified datasource to import.
   * @param array $options
   *   Options array.
   *
   * @command convivial_content:import
   *
   * @option int $cleanup
   *   Set this to delete existing content when importing new content.
   *   Additionally, this action will modify the basic site settings.
   *
   * @usage drush convivial_content-import bookshop
   *   Import the bookshop dataset content in the website without overriding
   *   the content on existing site.
   * @usage drush convivial_content-import bookshop --cleanup=1
   *   Import the bookshop dataset content in the website and also modify
   *   the existing site.
   * @usage drush convivial_content-import umami
   *   Import the umami dataset content in the website without overriding
   *   the content on existing site.
   * @usage drush convivial_content-import umami --cleanup=1
   *   Import the bookshop dataset content in the website and also modify
   *   the existing site.
   * @usage drush convivial_content-import news
   *   Import the news dataset content in the website without overriding
   *   the content on existing site.
   * @usage drush convivial_content-import news --cleanup=1
   *   Import the news dataset content in the website and also modify
   *   the existing site.
   *
   * @aliases convivial_content-import
   */
  public function import($dataset, $options = ['cleanup' => '0']) {
    if (!$this->io()->confirm(dt('Are you sure you want to import content from the datasource @dataset?', ['@dataset' => $dataset]))) {
      throw new UserAbortException();
    }
    $config = $this->configFactory->get('convivial_content.settings');
    $sourceUrl = $config->get('source_url');
    $datasetValue = $this->dataSourceManager->fetchDataset($sourceUrl, $dataset);
    $yaml = $this->dataSourceManager->getFileContent($sourceUrl, $datasetValue['file']);
    $result = $this->dataImporter->importContent($yaml, $sourceUrl, $options['cleanup']);
    $this->output()->writeln(dt('The import process for the @dataset dataset has been successfully completed.', ['@dataset' => $dataset]));
    $this->output()->writeln(dt('The following entities were imported during the process: @result ', ['@result' => implode(', ', $result)]));
  }

}
